<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id= "sessaoAtual" class="pratica.jsp.loginBean" scope="session"/>
<jsp:setProperty name="sessaoAtual" property="*" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <form method="post" action="">
            Código: <input type="text" name="login" value="${sessaoAtual.login}"/><br/>
            Nome: <input type="password" name="senha" value="${sessaoAtual.senha}"/><br/>
            Perfil: <select name="perfil">
                <option value="1">Cliente</option> 
                <option value="2">Gerente</option> 
                <option value="3">Administrador</option>
                 <select>
         <input type="submit" value="Enviar"/>
        </form> 
        
        <%
            //login valido 
            if(sessaoAtual.getLogin() != null){
                
                String login = sessaoAtual.getLogin();
                String senha = sessaoAtual.getSenha();
                Date date = sessaoAtual.getDate();
                
                if(login.equals(senha)){
                %>
                       <html>
                        <div>
                            <font color="blue"> <%=sessaoAtual.nomePerfil()%>, login bem sucedido, para <%=senha%> às hora <%=date%></font>
                        </div>
                    </html> 
                <%
                }
                //login inválido
                else{
                %>
                     <html>
                        <div>
                            <font color="red"> <i>Acesso negado</i></font>
                        </div>
                    </html> 
                <%
                }
            }
        %>
        
    </body>
</html>
