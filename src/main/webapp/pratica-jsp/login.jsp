<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Bem Vindo</h1>
        <% 
            String login="", senha="";
            int perfil=0;
            
            if(sessaoAtual.getLogin()!= null){
                login=sessaoAtual.getLogin();
            }
            if(sessaoAtual.getSenha()!= null){
                senha=sessaoAtual.getSenha();
            }
            if(sessaoAtual.getPerfil() != 0){
                perfil=sessaoAtual.getPerfil();
            }
        %>
        
        <jsp:useBean id= "sessaoAtual" class="pratica.jsp.loginBean" scope="session"/>
        <jsp:setProperty name="sessaoAtual" property="login" value="<%=login%>"/>
        <jsp:setProperty name="sessaoAtual" property="senha" value="<%=senha%>"/>
        <jsp:setProperty name="sessaoAtual" property="perfil" value="<%=perfil%>"/>
        
        <%
            //login valido
            if(login == senha){
            %>
                Login Válido
            <%
            }
            //login inválido
            else{
            %>
                Login Inválido
            <%
            }
        %>       
    </body>
</html>
