/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica.jsp;

import java.awt.event.ActionEvent;
import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class loginBean {

    //Variaveis
    String login;
    String senha;
    Date date;
    int perfil;
    String nomePerfil;

    //getteres e setteres
    public String getNomePerfil() {
        return nomePerfil;
    }

    public void setNomePerfil(String nomePerfil) {
        this.nomePerfil = nomePerfil;
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getPerfil() {
        return perfil;
    }

    public void setPerfil(int perfil) {
        this.perfil = perfil;
    }    
    
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public String nomePerfil(){
    
        if(perfil == 1){
            nomePerfil = "Cliente";
        }
        if(perfil == 2){
            nomePerfil = "Gerente";
        }
        if(perfil == 3){
            nomePerfil = "Administrador";
        }
        
        return nomePerfil; 
    
    }
    
    public Date buscarDataAtual(ActionEvent e) {  
       date = new Date();  
       return date;
    } 
    
}